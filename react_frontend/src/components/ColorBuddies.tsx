import { useState, useEffect, FC } from "react";

import "./ColorBuddies.scss";

const ColorBuddies: FC = () => {
  const [currentColor, setCurrentColor] = useState("");
  const [complementaryColors, setComplementaryColors] = useState([]);

  const getComplementaryColors = async (color: string) => {
    const response = await fetch("http://localhost:5000/complementary", {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ myColor: color }),
    });

    return response.json();
  };

  useEffect(() => {
    if (currentColor != "") {
      getComplementaryColors(currentColor).then((data) =>
        setComplementaryColors(data)
      );
    }
  }, [currentColor]);

  return (
    <div className={"app"}>
      <section className="title-container">
        <h1 className="title">Color Buddies</h1>
        <h3 className="subtitle">Pick A Color:</h3>
        <input
          className="color-input"
          type="color"
          onChange={(e) => setCurrentColor(e.target.value)}
        />
        <h3 className="subtitle">{currentColor}</h3>
      </section>
      <section className="colors-container">
        {complementaryColors.map((color, i) => (
          <div
            key={i}
            className="show-color"
            style={{ backgroundColor: color }}
          >
            {color}
          </div>
        ))}
      </section>
    </div>
  );
};

export default ColorBuddies;
