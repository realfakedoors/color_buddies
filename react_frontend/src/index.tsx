import React from "react";
import ReactDOM from "react-dom";

import ColorBuddies from "./components/ColorBuddies";

ReactDOM.render(
  <React.StrictMode>
    <ColorBuddies />
  </React.StrictMode>,
  document.getElementById("root")
);
