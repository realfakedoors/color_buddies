const express = require('express');
const cors = require('cors');

const tinycolor = require('tinycolor2');
// tinycolor's documentation:
// https://github.com/bgrins/TinyColor#readme

const api = express();
api.use(cors());
api.use(express.json());

api.post('/complementary', async (req, res) => {
  const inputColor = req.body.myColor;

  // tinycolor.splitcomplement() returns our inputColor as 
  // its first element, so we can just slice() it off.
  const complementaryColorData = tinycolor(inputColor).splitcomplement().slice(1);
  const hexColors = complementaryColorData.map(color => { return color.toHexString()});
  
  res.send(hexColors);
});

api.listen(5000, err => {
  console.log('listening on port 5000...')
});
