## intro:
- welcome and thank you for joining!
- take a look at the app code
- each of our services is going to get its own Dockerfile
- we're going to run multiple containers with docker-compose
- this is a very high-level tutorial. disclaimer

## Dockerfile for express_api:

```
FROM node:latest
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .
CMD ["npm", "start"]
```

- docker build .
- docker run -p 5000:5000 -it <image id>
- POST to localhost:5000/complementary in Postman with this JSON body:
```
{
  "myColor": "#bada55"
}
```

## notes on Dockerfiles:
- always starts with a FROM
- the INSTRUCTION is case-insensitive, but it's convention to capitalize
- RUN is for commands in the build process,
- while CMD is for providing the default command to execute when a container starts up.
- CMD = Highlander. there can only be one CMD

## other things you could specify in a Dockerfile:
- EXPOSE to specify container ports
- ENV for env vars
- LABEL to add metadata
- ENTRYPOINT if you want to configure a container to run as an executable
- USER if you want to create a user or a group of users at the container level
- ARG for build-time variables
- STOPSIGNAL for setting the system call that SIGTERMs the container
- HEALTHCHECK for telling Docker how to test that a container is running smoothly
- SHELL for specifying which native shell a command gets issued from

## Dockerfile for react_frontend:

- SWITCH TO react_frontend DIRECTORY!!!!!!!!

```
FROM node:latest
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .
CMD ["npm", "start"]
```

- docker build .
- if it fails with an `ERROR [internal] load metadata for docker.io/library/node:latest`, go into Docker Desktop -> Gear Icon -> Docker Engine -> buildkit to false -> apply & restart
- talk about volumes while the image builds
- docker run -p 3000:3000 -it <image id>
- visit `localhost:3000` in the browser

- `cd ..` back up to color_buddies

## docker-compose.yml:

```
version: '3'

services:
  react_frontend:
    build:
      context: ./react_frontend
      dockerfile: Dockerfile
    ports:
      - "3000:3000"
    volumes:
      - ./react_frontend:/app
  express_api:
    build:
      context: ./express_api
      dockerfile: Dockerfile
    ports:
      - "5000:5000"
```

- refer to docker_volumes_visualization.jpg
- if we wanted to bookmark our node_modules folder once it's installed in the container, since it's not in our local project directory, we would add `- ./node_modules` to our `volumes:`. But we installed the bundle locally so it wouldn't give us linter errors.
- the volume we use here tells the /app directory in the running container to refer to our own local project code.
- our image still gets built when we run `docker build` manually.

- docker-compose up
- localhost:3000
- pick your favorite color!!!

## other things you could specify in a `docker-compose.yml` file:
- environment: for env vars
- env_file: specify a file of env vars
- image: if a service uses an externally-sourced image
- args: buildtime variables
- dns: a list of custom DNS servers
- healthcheck: tests that containers aren't crashing or looping
